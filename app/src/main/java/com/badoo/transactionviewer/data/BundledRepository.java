package com.badoo.transactionviewer.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.badoo.transactionviewer.R;
import com.badoo.transactionviewer.TvApplication;
import com.badoo.transactionviewer.common.ResourceUtil;
import com.badoo.transactionviewer.products.Product;
import com.badoo.transactionviewer.transactions.Transaction;
import com.badoo.transactionviewer.transactions.TransactionProductParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Ragunath Jawahar
 */
public class BundledRepository extends Repository {
    private List<Transaction> transactions;
    private List<Product> products;

    public BundledRepository(@NonNull Context context) {
        super(context);
        String transactionsJson = ResourceUtil.readRawResource(context, TvApplication.TRANSACTIONS);
        TransactionProductParser transactionProductParser = new TransactionProductParser();

        if (transactionsJson != null) {
            Pair<? extends List<Transaction>, ? extends List<Product>> parsedData
                    = transactionProductParser.parse(transactionsJson);
            transactions = parsedData.first;
            products = parsedData.second;
        } else {
            transactions = Collections.EMPTY_LIST;
            products = Collections.EMPTY_LIST;
        }
    }

    @Override
    public List<Transaction> getTransactions(@NonNull final String sku) {
        List<Transaction> filteredTransactions = new ArrayList<>();
        for (int i = 0, n = transactions.size(); i < n; i++) {
            Transaction transaction = transactions.get(i);
            if (transaction != null && transaction.sku
                    != null && sku.equalsIgnoreCase(transaction.sku)) {
                filteredTransactions.add(transaction);
            }
        }
        return filteredTransactions;
    }

    @Override
    public List<Product> getProducts() {
        return products;
    }
}
