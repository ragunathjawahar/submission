package com.badoo.transactionviewer.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.badoo.transactionviewer.products.Product;
import com.badoo.transactionviewer.transactions.Transaction;

import java.util.List;

/**
 * @author Ragunath Jawahar
 */
public abstract class Repository {
    protected Context context;

    Repository(@NonNull Context context) {
        this.context = context;
    }

    public abstract List<Transaction> getTransactions(@NonNull String sku);
    public abstract List<Product> getProducts();
}
