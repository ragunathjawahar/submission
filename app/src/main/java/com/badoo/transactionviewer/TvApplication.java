package com.badoo.transactionviewer;

import android.app.Application;

import com.badoo.transactionviewer.dagger.AppComponent;
import com.badoo.transactionviewer.dagger.AppModule;
import com.badoo.transactionviewer.dagger.DaggerAppComponent;

/**
 * @author Ragunath Jawahar
 */
public class TvApplication extends Application {
    public static final int RATES = R.raw.rates_01;
    public static final int TRANSACTIONS = R.raw.transactions_01;

    private AppComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent component() {
        return applicationComponent;
    }
}
