package com.badoo.transactionviewer.dagger;

import com.badoo.transactionviewer.products.ProductsActivity;
import com.badoo.transactionviewer.transactions.Calculator;
import com.badoo.transactionviewer.transactions.TransactionsActivity;
import com.badoo.transactionviewer.transactions.TransactionsAdapter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Ragunath Jawahar
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(ProductsActivity productsActivity);
    void inject(TransactionsActivity transactionsActivity);

    void inject(Calculator calculator);
    void inject(TransactionsAdapter transactionsAdapter);
}
