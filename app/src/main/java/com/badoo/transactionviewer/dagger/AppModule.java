package com.badoo.transactionviewer.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import com.badoo.transactionviewer.currency.CurrencyConverter;
import com.badoo.transactionviewer.data.BundledRepository;
import com.badoo.transactionviewer.data.Repository;
import com.badoo.transactionviewer.transactions.Calculator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Ragunath Jawahar
 */
@Module
public class AppModule {

    private final Context context;

    public AppModule(@NonNull Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Repository provideRepository() {
        return new BundledRepository(context);
    }

    @Provides
    @Singleton
    CurrencyConverter provideCurrencyConverter() {
        return new CurrencyConverter(context);
    }

    @Provides
    @Singleton
    Calculator provideCalculator() {
        return new Calculator(context);
    }
}
