package com.badoo.transactionviewer.products;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.badoo.transactionviewer.R;
import com.badoo.transactionviewer.TvApplication;
import com.badoo.transactionviewer.data.Repository;
import com.badoo.transactionviewer.transactions.TransactionsActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

/**
 * @author Ragunath Jawahar
 */
public class ProductsActivity extends AppCompatActivity {
    public static final String EXTRA_SKU = "EXTRA_SKU";

    @Inject Repository repository;

    @BindView(R.id.productsListView) ListView productsListView;
    @BindView(android.R.id.empty) View emptyView;

    private ListAdapter productsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        // Dagger
        ((TvApplication) getApplication()).component().inject(this);

        // UI
        setTitle(R.string.title_products);
        ButterKnife.bind(this);
        initListView();
    }

    @OnItemClick(R.id.productsListView)
    void showTransaction(int position) {
        Product product = (Product) productsAdapter.getItem(position);

        Intent intent = new Intent(this, TransactionsActivity.class);
        intent.putExtra(EXTRA_SKU, product.sku);
        startActivity(intent);
    }

    private void initListView() {
        List<Product> products = repository.getProducts();
        productsListView.setEmptyView(emptyView);
        productsAdapter = new ProductsAdapter(this, products);
        productsListView.setAdapter(productsAdapter);
    }
}
