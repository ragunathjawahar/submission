package com.badoo.transactionviewer.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.badoo.transactionviewer.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ragunath Jawahar
 */
public class ProductsAdapter extends ArrayAdapter<Product> {
    private LayoutInflater layoutInflater;

    public ProductsAdapter(@NonNull Context context, @NonNull List<Product> objects) {
        super(context, R.layout.list_item_2_line, objects);
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_2_line, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Product product = getItem(position);
        viewHolder.set(product);

        return view;
    }

    static class ViewHolder {
        private Context context;

        @BindView(R.id.titleTextView) TextView skuTextView;
        @BindView(R.id.subtitleTextView) TextView transactionsTextView;

        ViewHolder(@NonNull View view) {
            ButterKnife.bind(this, view);
            context = view.getContext();
        }

        void set(@NonNull Product product) {
            skuTextView.setText(product.sku);
            String transactions = context.getString(
                    R.string.format_n_transactions, product.transactions);
            transactionsTextView.setText(transactions);
        }
    }
}
