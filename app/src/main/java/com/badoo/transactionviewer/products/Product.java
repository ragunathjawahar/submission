package com.badoo.transactionviewer.products;

/**
 * @author Ragunath Jawahar
 */
public class Product {
    public String sku;
    public int transactions;

    public Product(String sku, int transactions) {
        this.sku = sku;
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return "Product{" +
                "sku='" + sku + '\'' +
                ", transactions=" + transactions +
                '}';
    }
}
