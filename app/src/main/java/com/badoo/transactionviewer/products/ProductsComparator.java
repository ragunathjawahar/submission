package com.badoo.transactionviewer.products;

import java.util.Comparator;

/**
 * @author Ragunath Jawahar
 */
public class ProductsComparator implements Comparator<Product> {

    @Override
    public int compare(Product lhs, Product rhs) {
        if (lhs == null && rhs == null) {
            return 0;
        } else if (lhs == null) {
            return -1;
        } else if (rhs == null) {
            return 1;
        }

        return lhs.sku != null
                ? lhs.sku.compareTo(rhs.sku) : rhs.sku != null ? -1 : 0;
    }
}
