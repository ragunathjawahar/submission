package com.badoo.transactionviewer.currency;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.badoo.transactionviewer.R;
import com.badoo.transactionviewer.TvApplication;
import com.badoo.transactionviewer.common.ResourceUtil;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * @author Ragunath Jawahar
 */
public class CurrencyConverter {
    public static final Currency DEFAULT_CURRENCY = new Currency("GBP");
    private static final BigDecimal ZERO_RATE = new BigDecimal(0);

    // Parsed from source
    private final Set<Currency> currencies;
    private final Map<Currency, Map<Currency, BigDecimal>> knownRatesMap;

    private final Map<Currency, String> symbolsMap;
    private final Map<Currency, BigDecimal> rateMap;

    public CurrencyConverter(@NonNull Context context) {
        String rateJson = ResourceUtil.readRawResource(context, TvApplication.RATES);
        RateCurrencyParser parser = new RateCurrencyParser();
        Pair<? extends List<Rate>, ? extends List<Currency>> pair = parser.parse(rateJson);

        // Currencies
        currencies = Collections.unmodifiableSet(new HashSet<>(pair.second));

        // This one contains all rates that have direct conversions to the default currency.
        rateMap = new HashMap<>();

        // Known rates
        List<Rate> knownRates = pair.first;
        knownRatesMap = getKnownRates(knownRates);

        // Symbols
        symbolsMap = getCurrencySymbols(context);
    }

    @NonNull
    private Map<Currency, String> getCurrencySymbols(@NonNull Context context) {
        Map<Currency, String> currencySymbolsMap = new HashMap<>();
        String[] currenciesSymbols = context.getResources()
                .getStringArray(R.array.currencies_symbols);
        for (int i = 0, n = currenciesSymbols.length; i < n; i++) {
            String[] currencySymbol = currenciesSymbols[i].split(":");
            currencySymbolsMap.put(new Currency(currencySymbol[0]), currencySymbol[1]);
        }
        return Collections.unmodifiableMap(currencySymbolsMap);
    }

    public String format(@NonNull Currency currency, @NonNull BigDecimal amount) {
        String symbol = symbolsMap.get(currency) == null
                ? currency.name : symbolsMap.get(currency);
        String formattedAmount = String.format(Locale.ENGLISH, "%,.2f", amount);
        return String.format("%s%s", symbol, formattedAmount);
    }

    public BigDecimal toDefaultCurrency(@NonNull Currency currency, @NonNull BigDecimal amount) {
        BigDecimal convertedAmount;
        if (DEFAULT_CURRENCY.equals(currency)) {
            convertedAmount = amount;
        } else {
            BigDecimal conversionRate = getConversionRate(currency, DEFAULT_CURRENCY);
            convertedAmount = amount.multiply(conversionRate)
                    .setScale(2, BigDecimal.ROUND_HALF_DOWN);
        }
        return convertedAmount;
    }

    @NonNull
    private Map<Currency, Map<Currency, BigDecimal>> getKnownRates(List<Rate> knownRates) {
        Map<Currency, Map<Currency, BigDecimal>> knownRatesMap = new HashMap<>();
        for (int i = 0, n = knownRates.size(); i < n; i++) {
            Rate rate = knownRates.get(i);
            Currency fromCurrency = rate.from;

            Map<Currency, BigDecimal> toCurrencyRateMap = knownRatesMap.get(fromCurrency);
            if (toCurrencyRateMap == null) {
                toCurrencyRateMap = new HashMap<>();
                knownRatesMap.put(fromCurrency, toCurrencyRateMap);
            }
            toCurrencyRateMap.put(rate.to, rate.rate);

            if (rate.to.equals(DEFAULT_CURRENCY)) {
                rateMap.put(rate.from, rate.rate);
            }
        }
        return Collections.unmodifiableMap(knownRatesMap);
    }

    private BigDecimal getConversionRate(@NonNull Currency fromCurrency,
            @NonNull Currency toCurrency) {
        BigDecimal conversionRate = rateMap.get(fromCurrency);
        if (conversionRate == null) {
            conversionRate = findConversionRate(fromCurrency, toCurrency);
            rateMap.put(fromCurrency, conversionRate);
        }

        return conversionRate;
    }

    private BigDecimal findConversionRate(@NonNull Currency fromCurrency,
            @NonNull Currency toCurrency) {

        // Populate the map for visiting
        PriorityQueue<CurrencyRatePair> unvisitedCurrencies = new PriorityQueue<>();
        for (Currency currency : currencies) {
            if (!fromCurrency.equals(currency)) {
                unvisitedCurrencies.add(new CurrencyRatePair(currency, ZERO_RATE));
            }
        }

        // This will contain all discovered conversion rates from the source currency to all
        // other currencies.
        Map<Currency, BigDecimal> rateMap = new HashMap<>();
        rateMap.put(fromCurrency, new BigDecimal(1));

        Currency currentCurrency = fromCurrency;
        PriorityQueue<CurrencyRatePair> dirtyFixQueue = new PriorityQueue<>();
        while (currentCurrency != null) {
            Map<Currency, BigDecimal> neighbors = getNeighbors(currentCurrency);

            Set<Currency> neighborCurrencies = neighbors.keySet();
            for (Currency neighborCurrency : neighborCurrencies) {

                // Bottle-neck, replace with a HashMap implementation and remove dirty-fix queue
                for (CurrencyRatePair unvisitedCurrencyPair : unvisitedCurrencies) {
                    if (neighborCurrency.equals(unvisitedCurrencyPair.first)) {
                        BigDecimal currentCurrencyRate = rateMap.get(currentCurrency);
                        unvisitedCurrencyPair.second = currentCurrencyRate == null
                                ? neighbors.get(neighborCurrency)
                                : neighbors.get(neighborCurrency).multiply(currentCurrencyRate);
                    }
                }
            }

            if (!unvisitedCurrencies.isEmpty()) {
                for (CurrencyRatePair unvisitedCurrency : unvisitedCurrencies) {
                    dirtyFixQueue.add(unvisitedCurrency);
                }
                PriorityQueue<CurrencyRatePair> temp = dirtyFixQueue;
                dirtyFixQueue = unvisitedCurrencies;
                unvisitedCurrencies = temp;

                dirtyFixQueue.clear();

                CurrencyRatePair currencyRatePair = unvisitedCurrencies.poll();
                currentCurrency = currencyRatePair.first;
                rateMap.put(currencyRatePair.first, currencyRatePair.second);
            } else {
                currentCurrency = null;
            }
        }

        return rateMap.get(toCurrency);
    }

    private Map<Currency, BigDecimal> getNeighbors(@NonNull Currency currency) {
        return knownRatesMap.get(currency);
    }

    private static class CurrencyRatePair extends MutablePair<Currency, BigDecimal>
            implements Comparable<CurrencyRatePair> {

        public CurrencyRatePair(@NonNull Currency first, @NonNull BigDecimal second) {
            super(first, second);
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof CurrencyRatePair && first.equals(((CurrencyRatePair) o).first);
        }

        @Override
        public int compareTo(@NonNull CurrencyRatePair another) {
            return this.second.compareTo(another.second) * -1;
        }
    }

    /**
     * A mutable version of {@link android.support.v4.util.Pair}.
     */
    private static class MutablePair<F, S> {
        public F first;
        public S second;

        public MutablePair(F first, S second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof MutablePair)) {
                return false;
            }
            MutablePair<?, ?> p = (MutablePair<?, ?>) o;
            return objectsEqual(p.first, first) && objectsEqual(p.second, second);
        }

        private static boolean objectsEqual(Object a, Object b) {
            return a == b || (a != null && a.equals(b));
        }

        @Override
        public int hashCode() {
            return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode());
        }

        @Override
        public String toString() {
            return "MutablePair{" +
                    "first=" + first +
                    ", second=" + second +
                    '}';
        }

        public static <A, B> MutablePair<A, B> create(A a, B b) {
            return new MutablePair<>(a, b);
        }
    }
}
