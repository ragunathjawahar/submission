package com.badoo.transactionviewer.currency;

import android.support.annotation.NonNull;

/**
 * @author Ragunath Jawahar
 */
public class Currency {
    public final String name;

    public Currency(@NonNull  String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Currency currency = (Currency) o;

        return name.equals(currency.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Currency{" +
                "name='" + name + '\'' +
                '}';
    }
}
