package com.badoo.transactionviewer.currency;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.util.Log;

import com.badoo.transactionviewer.common.PairObjectParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ragunath Jawahar
 */
class RateCurrencyParser extends PairObjectParser<Rate, Currency> {

    public static final String TAG = "RateCurrencyParser";

    @Override
    public Pair<? extends List<Rate>, ? extends List<Currency>> parse(@NonNull String json) {
        Pair<? extends List<Rate>, ? extends List<Currency>> pair =
                Pair.create(new ArrayList<Rate>(), new ArrayList<Currency>());

        try {
            JSONArray jsonArray = new JSONArray(json);

            int length = jsonArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                boolean valid = jsonObject.has("from") && jsonObject.has("to")
                        && jsonObject.has("rate");

                if (valid) {
                    Currency from = new Currency(jsonObject.getString("from").toUpperCase());
                    Currency to = new Currency(jsonObject.getString("to").toUpperCase());
                    BigDecimal rate = new BigDecimal(jsonObject.getString("rate"))
                            .setScale(2, BigDecimal.ROUND_DOWN);
                    pair.first.add(new Rate(from, to, rate));

                    if (!pair.second.contains(from)) {
                        pair.second.add(from);
                    }
                    if (!pair.second.contains(to)) {
                        pair.second.add(to);
                    }
                } else {
                    // Better to log this information without crashing in production
                    Log.w(TAG, jsonObject.toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pair;
    }
}
