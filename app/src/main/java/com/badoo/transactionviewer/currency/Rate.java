package com.badoo.transactionviewer.currency;

import android.support.annotation.NonNull;

import java.math.BigDecimal;

/**
 * @author Ragunath Jawahar
 */
public class Rate {
    public final Currency from;
    public final Currency to;
    public final BigDecimal rate;

    public Rate(@NonNull Currency from, @NonNull Currency to, @NonNull BigDecimal rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "from=" + from +
                ", to=" + to +
                ", rate=" + rate +
                '}';
    }
}
