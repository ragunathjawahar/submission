package com.badoo.transactionviewer.transactions;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.util.Log;

import com.badoo.transactionviewer.common.PairObjectParser;
import com.badoo.transactionviewer.products.Product;
import com.badoo.transactionviewer.products.ProductsComparator;
import com.badoo.transactionviewer.transactions.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Ragunath Jawahar
 */
public class TransactionProductParser extends PairObjectParser<Transaction, Product> {
    // Suppress lint warning (23 char limit)
    public static final String TAG = "TransactionProductParser".substring(0, 23);

    public Pair<? extends List<Transaction>, ? extends List<Product>> parse(@NonNull String json) {
        Pair<? extends List<Transaction>, ? extends List<Product>> pair =
                Pair.create(new ArrayList<Transaction>(), new ArrayList<Product>());
        try {
            JSONArray jsonArray = new JSONArray(json);
            Map<String, Integer> productsMap = new HashMap<>();

            // Transactions
            populateTransactions(jsonArray, pair.first, productsMap);

            // Products
            populateProducts(productsMap, pair.second);
            Collections.sort(pair.second, new ProductsComparator());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return pair;
    }

    private void populateTransactions(
            @NonNull JSONArray jsonArray,
            @NonNull List<Transaction> outTransactions,
            @NonNull Map<String, Integer> outProductsMap) throws JSONException {

        int length = jsonArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            boolean validJson = jsonObject.has("sku") && jsonObject.has("currency")
                    && jsonObject.has("amount");
            if (validJson) {
                String sku = jsonObject.getString("sku");
                String currency = jsonObject.getString("currency");
                String amount = jsonObject.getString("amount");

                if (outProductsMap.containsKey(sku)) {
                    Integer transactionCount = outProductsMap.get(sku);
                    transactionCount++;
                    outProductsMap.put(sku, transactionCount);
                } else {
                    outProductsMap.put(sku, 1);
                }

                BigDecimal bigDecimal = new BigDecimal(amount).setScale(2, BigDecimal.ROUND_FLOOR);
                outTransactions.add(new Transaction(sku, currency, bigDecimal));
            } else {
                // Better to log this information without crashing in production
                Log.w(TAG, jsonObject.toString());
            }
        }
    }

    private void populateProducts(@NonNull Map<String, Integer> inProductsMap,
            @NonNull List<Product> outProducts) {
        Set<String> skus = inProductsMap.keySet();
        for (String sku : skus) {
            outProducts.add(new Product(sku, inProductsMap.get(sku)));
        }
    }
}
