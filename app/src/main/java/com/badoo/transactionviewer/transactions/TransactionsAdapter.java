package com.badoo.transactionviewer.transactions;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.badoo.transactionviewer.R;
import com.badoo.transactionviewer.TvApplication;
import com.badoo.transactionviewer.currency.CurrencyConverter;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ragunath Jawahar
 */
public class TransactionsAdapter extends ArrayAdapter<Transaction> {

    @Inject CurrencyConverter converter;

    private LayoutInflater layoutInflater;

    public TransactionsAdapter(@NonNull Context context, @NonNull List<Transaction> objects) {
        super(context, R.layout.list_item_2_line, objects);
        ((TvApplication) context.getApplicationContext()).component().inject(this);
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_2_line, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Transaction transaction = getItem(position);
        viewHolder.set(converter, transaction);

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.titleTextView) TextView transactionCurrencyTextView;
        @BindView(R.id.subtitleTextView) TextView defaultCurrencyTextView;

        ViewHolder(@NonNull View view) {
            ButterKnife.bind(this, view);
        }

        void set(@NonNull CurrencyConverter converter, @NonNull Transaction transaction) {
            String transactionCurrency = converter.format(transaction.currency, transaction.amount);
            transactionCurrencyTextView.setText(transactionCurrency);

            BigDecimal convertedAmount = converter.toDefaultCurrency(
                    transaction.currency, transaction.amount);
            defaultCurrencyTextView.setText(converter.format(
                    CurrencyConverter.DEFAULT_CURRENCY, convertedAmount));
        }
    }
}
