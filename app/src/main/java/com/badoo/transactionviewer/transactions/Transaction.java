package com.badoo.transactionviewer.transactions;

import android.support.annotation.NonNull;

import com.badoo.transactionviewer.currency.Currency;

import java.math.BigDecimal;

/**
 * @author Ragunath Jawahar
 */
public class Transaction {
    public String sku;
    public Currency currency;
    public BigDecimal amount;

    public Transaction(@NonNull String sku, @NonNull String currency, @NonNull BigDecimal amount) {
        this.sku = sku;
        this.currency = new Currency(currency);
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "sku='" + sku + '\'' +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                '}';
    }
}
