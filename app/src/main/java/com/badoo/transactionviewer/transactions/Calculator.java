package com.badoo.transactionviewer.transactions;

import android.content.Context;
import android.support.annotation.NonNull;

import com.badoo.transactionviewer.TvApplication;
import com.badoo.transactionviewer.currency.CurrencyConverter;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Ragunath Jawahar
 */
public class Calculator {
    @Inject CurrencyConverter converter;

    public Calculator(@NonNull Context context) {
        ((TvApplication) context).component().inject(this);
    }

    public BigDecimal total(@NonNull List<Transaction> transactions) {
        BigDecimal total = new BigDecimal(0);
        for (int i = 0, n = transactions.size(); i < n; i++) {
            Transaction transaction = transactions.get(i);
            BigDecimal convertedAmount = converter.toDefaultCurrency(
                    transaction.currency, transaction.amount);
            total = total.add(convertedAmount);
        }
        return total;
    }
}
