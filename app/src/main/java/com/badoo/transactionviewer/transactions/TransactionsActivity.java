package com.badoo.transactionviewer.transactions;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import com.badoo.transactionviewer.R;
import com.badoo.transactionviewer.TvApplication;
import com.badoo.transactionviewer.currency.Currency;
import com.badoo.transactionviewer.currency.CurrencyConverter;
import com.badoo.transactionviewer.data.Repository;
import com.badoo.transactionviewer.products.ProductsActivity;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ragunath Jawahar
 */
public class TransactionsActivity extends AppCompatActivity {

    @Inject Repository repository;
    @Inject Calculator calculator;
    @Inject CurrencyConverter converter;

    @BindView(R.id.totalTextView) TextView totalTextView;
    @BindView(R.id.transactionsListView) ListView transactionsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        // Dagger
        ((TvApplication) getApplication()).component().inject(this);

        // Extras
        Bundle extras = getIntent().getExtras();
        String sku = extras.getString(ProductsActivity.EXTRA_SKU);

        // UI
        setTitle(getString(R.string.title_transactions, sku));
        ButterKnife.bind(this);
        initUi(sku);
    }

    private void initUi(String sku) {
        // ListView
        List<Transaction> transactions = repository.getTransactions(sku);
        TransactionsAdapter transactionsAdapter = new TransactionsAdapter(this, transactions);
        transactionsListView.setAdapter(transactionsAdapter);

        // Total
        BigDecimal total = calculator.total(transactions);
        String formattedTotal = converter.format(CurrencyConverter.DEFAULT_CURRENCY, total);
        totalTextView.setText(getString(R.string.format_total, formattedTotal));
    }
}
