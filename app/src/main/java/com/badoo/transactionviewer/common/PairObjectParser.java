package com.badoo.transactionviewer.common;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import java.util.List;

/**
 * @author Ragunath Jawahar
 */
public abstract class PairObjectParser<A, B> {
    public abstract Pair<? extends List<A>, ? extends List<B>> parse(@NonNull String json);
}
