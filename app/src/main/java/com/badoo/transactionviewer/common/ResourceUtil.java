package com.badoo.transactionviewer.common;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import okio.BufferedSource;
import okio.Okio;

/**
 * @author Ragunath Jawahar
 */
public class ResourceUtil {

    public static String readRawResource(@NonNull Context context, @RawRes int resourceId) {
        Resources resources = context.getResources();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BufferedSource bufferedSource = Okio.buffer(
                Okio.source(resources.openRawResource(resourceId)));
        try {
            bufferedSource.readAll(Okio.sink(outputStream));
            return outputStream.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try { outputStream.close(); } catch (IOException ignored) {}
            try { bufferedSource.close(); } catch (IOException ignored) {}
        }
        return null;
    }

    private ResourceUtil() {
        throw new IllegalStateException("No instances.");
    }
}
